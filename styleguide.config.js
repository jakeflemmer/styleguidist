/** @format */
const path = require('path')
module.exports = {
  require: [
    'babel-polyfill',
    path.join(__dirname, '/src/fontAwesome/index.js'),
    path.join(__dirname, 'node_modules/normalize.css/normalize.css'),
    path.join(__dirname, 'node_modules/@blueprintjs/icons/lib/css/blueprint-icons.css'),
    path.join(__dirname, 'node_modules/@blueprintjs/core/lib/css/blueprint.css')
  ],
  components: 'src/components/**/[A-Z]*.jsx'
}
