/* eslint-disable no-unused-vars */
import React from 'react'
import { library } from '@fortawesome/fontawesome-svg-core'
// 1. Import the icon you want from either the FAS, FAB, or FAR libraries

// FAS
import * as fas from '@fortawesome/pro-solid-svg-icons'

// FAR
import * as far from '@fortawesome/pro-regular-svg-icons'

// FAL
import * as light from '@fortawesome/pro-light-svg-icons'

// // FAD
// import * as duo from '@fortawesome/pro-duotone-svg-icons';

// 2. Add it to the library
library.add(
  far.faEllipsisH,
  far.faPlus,
  far.faPlusCircle,
  far.faFileImport,

  fas.faCaretDown,
  far.faFile,
  fas.faChartNetwork,
  fas.faCircle,
  fas.faCube,
  fas.faGlobe,
  fas.faLayerGroup,
  fas.faPencil,
  fas.faProjectDiagram,
  fas.faQuestionCircle,
  fas.faStar,
  fas.faUserFriends,
  fas.faPlusCircle,
  fas.faShapes,
  fas.faChevronDown,
  fas.faArrowUp,
  fas.faArrowDown,
  fas.faLink,
  fas.faClone,
  fas.faCopy,
  fas.faTrash,
  fas.faEye,
  fas.faCog,
  fas.faDatabase,
  fas.faLock,
  far.faTags,
  fas.faMagic,
  fas.faCaretDown,
  fas.faPalette,
  fas.faBandAid,
  fas.faFile,
  fas.faExclamationSquare,
  fas.faExclamationTriangle,
  fas.faCheckCircle,
  fas.faInfoCircle,
  fas.faEllipsisH,
  fas.faFolderTree,
  fas.faRocketLaunch,
  fas.faFileCsv,
  fas.faChevronRight,
  fas.faCaretDown,
  fas.faBringForward,
  fas.faRedo,

  light.faTimes
)

/** @type import('react').FC */
export const FontAwesome = ({ children }) => <>{children}</>
