/** @format */

import React from 'react'
import PropTypes from 'prop-types'
import { Breadcrumbs as BPBreadcumbs, Breadcrumb } from '@blueprintjs/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { CurrentBreadcrumb } from './CurrentBreadcrumb'

import './styles.scss'

/** @type {import('./types').IBreadcrumbs} */

export const Breadcrumbs = ({ items, handleClick, children }) => {
  return (
    <span className="breadcrumb-nav">
      <BPBreadcumbs
        items={items}
        breadcrumbRenderer={(bcrProps) => BreadcrumbRenderer(bcrProps, handleClick)}
        currentBreadcrumbRenderer={({ href, icon, text }) => <CurrentBreadcrumb {...{ href, icon, text, children }} />}
      />
    </span>
  )
}

export const BreadcrumbRenderer = ({ href, text, icon }, handleClick) => (
  <Breadcrumb icon={icon && <FontAwesomeIcon icon={icon} fixedWidth />} onClick={() => handleClick(href)}>
    <span className="parent"> {text}</span>
  </Breadcrumb>
)

Breadcrumbs.defaultProps = {
  handleClick: () => {}
}

Breadcrumbs.propTypes = {
  items: PropTypes.array,
  handleClick: PropTypes.func,
  // it can receive a BreadcrumbMenu as a child
  children: PropTypes.node
}
