/** @format */
import React from 'react'
import { CurrentBreadcrumb } from './CurrentBreadcrumb'
import { fireEvent, render, cleanup } from '@testing-library/react'
import { mount } from 'enzyme'

const mockChildren = () => <div>hello world</div>
const mockClickHandler = jest.fn((href) => {})
const mockProps = {
  href: 'some/path/1',
  icon: 'layer-group',
  text: 'label 1',
  handleClick: mockClickHandler
}

describe('CurrentBreadcrumb', () => {
  afterEach(cleanup)

  it('renders the correct icon based on the icon string prop it received', () => {
    const wrapper = mount(CurrentBreadcrumb(mockProps))
    expect(wrapper.find('FontAwesomeIcon').length).toEqual(1)
    const icon = wrapper.find('FontAwesomeIcon').first()
    expect(icon.find({ icon: 'layer-group' }).length).toEqual(1)
  })

  it('should render a blue icon since its the last and thus current breadcrumb', () => {
    const { getByRole } = render(CurrentBreadcrumb(mockProps))
    const icon = getByRole('img', { hidden: true })
    expect(icon.classList.contains('current-icon')).toBe(true)
  })

  it('renders the correct label based on the text prop it receives', () => {
    const { getByText } = render(CurrentBreadcrumb(mockProps))
    const label = getByText(/label 1/i)
    expect(label).toBeInTheDocument()
  })

  it('calls its handleClick callback when clicked and passes its href prop to that func', () => {
    const { getByText } = render(CurrentBreadcrumb(mockProps))
    const label = getByText(/label 1/i)
    expect(mockClickHandler.mock.calls.length).toBe(0)
    fireEvent.click(label)
    expect(mockClickHandler.mock.calls.length).toBe(1)
    // the first argument of the first call
    expect(mockClickHandler.mock.calls[0][0]).toEqual('some/path/1')
  })

  it('if it is passed a children prop then it renders a drop down caret after the breadcrumb name', () => {
    const { getAllByRole } = render(CurrentBreadcrumb({ ...mockProps, ...{ children: mockChildren } }))
    const icon = getAllByRole('img', { hidden: true })[1]
    expect(icon.classList.contains('fa-caret-down')).toBe(true)
  })
})
