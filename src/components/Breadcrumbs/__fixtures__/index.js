/** @format */

export const BreadcrumbMenuData = [
  { href: '/0', icon: 'user-friends', text: 'Gorgeous Concrete Car' },
  { href: '/1', icon: 'pencil', text: 'Default Workspace' },
  { href: '/2', icon: 'user-friends', text: 'Ergonomic Cotton Shirt' },
  { href: '/3', icon: 'user-friends', text: 'Rustic Cotton Chair' },
  { href: '/4', icon: 'user-friends', text: 'Intelligent Cotton Sausages' },
  { href: '/5', icon: 'user-friends', text: 'Gorgeous Fresh Shirt' },
  { href: '/6', icon: 'user-friends', text: 'Generic Fresh Mouse' },
  { href: '/7', icon: 'user-friends', text: 'Incredible Cotton Car' },
  { href: '/8', icon: 'user-friends', text: 'Fantastic Frozen Chair' },
  { href: '/9', icon: 'user-friends', text: 'Incredible Granite Table' },
  { href: '/10', icon: 'user-friends', text: 'Gorgeous Frozen Mouse' },
  { href: '/11', icon: 'user-friends', text: 'Handmade Wooden Computer' },
  { href: '/12', icon: 'user-friends', text: 'Tasty Plastic Bacon' },
  { href: '/13', icon: 'user-friends', text: 'Unbranded Rubber Table' }
]

export const BREADCRUMBS = [
  { href: '/workspaces', icon: 'globe', text: 'Neue Company' },
  { href: '/workspaces/23', icon: 'user-friends', text: 'Acme' },
  { icon: 'layer-group', text: 'Acme General Application' }
]
