```jsx
import { BreadcrumbsMenu } from './Menu/BreadcrumbsMenu';

<CurrentBreadcrumb
  href='some/path' icon='layer-group' text='breadcrumb label' >
</CurrentBreadcrumb>
```

when it has a menu it shows a drop-down caret

```jsx
import { BreadcrumbsMenu } from './Menu/BreadcrumbsMenu';

<CurrentBreadcrumb
  href='some/path' icon='layer-group' text='breadcrumb label' >
  <BreadcrumbsMenu
    items={[
      { icon: 'pencil', text: 'a menu item' },
      { icon: 'pencil', text: 'a menu item' },
      { icon: 'pencil', text: 'a menu item' }
    ]}
  />
</CurrentBreadcrumb>
```
