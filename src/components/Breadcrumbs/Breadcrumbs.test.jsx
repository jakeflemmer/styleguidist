/** @format */
import { BreadcrumbRenderer } from './Breadcrumbs'
import { fireEvent, render, cleanup } from '@testing-library/react'
import { mount } from 'enzyme'

const mockItems = [
  { href: 'some/path/1', icon: 'layer-group', text: 'label 1' },
  { href: 'some/path/2', icon: 'pencil', text: 'label 2' },
  { href: 'some/path/3', icon: 'globe', text: 'label 3' }
]

const mockClickHandler = jest.fn()

describe('Breadcrumbs', () => {
  afterEach(cleanup)

  // had to fall back on Enzyme testing here as BluePrintJs wraps its icon its own way
  // and doesn't give a data-testid specifically for the icon
  it('renders an icon for each item it is passed with an icon property', () => {
    const wrapper1 = mount(BreadcrumbRenderer(mockItems[0], () => {}))
    expect(wrapper1.find('FontAwesomeIcon').length).toEqual(1)
    const icon1 = wrapper1.find('FontAwesomeIcon').first()
    expect(icon1.find({ icon: 'layer-group' }).length).toEqual(1)

    const wrapper2 = mount(BreadcrumbRenderer(mockItems[1], () => {}))
    expect(wrapper2.find('FontAwesomeIcon').length).toEqual(1)
    const icon2 = wrapper2.find('FontAwesomeIcon').first()
    expect(icon2.find({ icon: 'pencil' }).length).toEqual(1)
  })

  it('renders a label for each item it is passed with a label property', () => {
    const { getByText } = render(BreadcrumbRenderer(mockItems[0], () => {}))
    const label = getByText(/label 1/i)
    expect(label).toBeInTheDocument()

    const result = render(BreadcrumbRenderer(mockItems[1], () => {}))
    const label2 = result.getByText(/label 2/i)
    expect(label2).toBeInTheDocument()
  })

  it('calls the clickHandler callback function it is passed when clicked', () => {
    const { getByText } = render(BreadcrumbRenderer(mockItems[0], mockClickHandler))
    const label = getByText(/label 1/i)
    expect(mockClickHandler.mock.calls.length).toBe(0)
    fireEvent.click(label)
    expect(mockClickHandler.mock.calls.length).toBe(1)
    // the first argument of the first call
    expect(mockClickHandler.mock.calls[0][0]).toEqual('some/path/1')
  })

  it('each item passes its correct href prop value to the clickHandler prop function on click', () => {
    mockClickHandler.mockClear()
    const { getByText } = render(BreadcrumbRenderer(mockItems[0], mockClickHandler))
    const label = getByText(/label 1/i)
    fireEvent.click(label)
    // the first argument of the first call
    expect(mockClickHandler.mock.calls[0][0]).toEqual('some/path/1')

    const result = render(BreadcrumbRenderer(mockItems[1], mockClickHandler))
    const label2 = result.getByText(/label 2/i)
    fireEvent.click(label2)
    // the first argument of the second call
    expect(mockClickHandler.mock.calls[1][0]).toEqual('some/path/2')
  })
})
