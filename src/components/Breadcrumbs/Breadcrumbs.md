```jsx
import { BreadcrumbsMenu } from './Menu/BreadcrumbsMenu';

const bcItems = [
  { href: '/some/path', icon: 'globe', text: 'a Company' },
  { href: '/some/path', icon: 'pencil', text: 'a work space'},
  { href: '/some/path', icon: 'layer-group', text: 'a no-code application' }
];

const menuItems = [
  { href: 'some/path', icon: 'layer-group', text: 'a menu item' },
  { href: 'some/path', icon: 'layer-group', text: 'a menu item' },
  { href: 'some/path', icon: 'layer-group', text: 'a menu item' },
  { href: 'some/path', icon: 'layer-group', text: 'a menu item' },
  { href: 'some/path', icon: 'layer-group', text: 'a menu item' },
  { href: 'some/path', icon: 'layer-group', text: 'a menu item' }
];

<Breadcrumbs
  items={bcItems}
>
  <BreadcrumbsMenu
    items={menuItems}
  />
</Breadcrumbs>
```
