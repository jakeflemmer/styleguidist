/** @format */
import React from 'react'
import { BreadcrumbsMenu } from './BreadcrumbsMenu'
import { fireEvent, render, cleanup, screen } from '@testing-library/react'

const items = [
  { href: '/workspaces/wsid1', icon: 'layer-group', text: 'nocodeapp1 ' },
  { href: '/workspaces/wsid2', icon: 'pencil', text: 'nocodeapp2' },
  { href: '/workspaces/wsid3', icon: 'globe', text: 'nocodeapp3' }
]
const mockClickHandler = jest.fn()

describe('BreadcrumbsMenu', () => {
  afterEach(cleanup)
  it('takes an items array and renders a Menu.Item for each item in the array', () => {
    const { getByText } = render(<BreadcrumbsMenu items={items} />)
    const label1 = getByText(/nocodeapp1/i)
    const label2 = getByText(/nocodeapp2/i)
    const label3 = getByText(/nocodeapp3/i)
    expect(label1).toBeInTheDocument()
    expect(label2).toBeInTheDocument()
    expect(label3).toBeInTheDocument()
  })

  it('renders the correct icon for each menu item in the array', () => {
    const { getAllByRole } = render(<BreadcrumbsMenu items={items} />)
    const icons = getAllByRole('img', { hidden: true })
    expect(icons.length).toBe(3)
    expect(icons[0].classList.contains('fa-layer-group')).toBe(true)
    expect(icons[1].classList.contains('fa-pencil')).toBe(true)
    expect(icons[2].classList.contains('fa-globe')).toBe(true)
  })

  it('calls the handleClick callback function and passes the correct href for each menu item clicked', () => {
    const { getByText } = render(<BreadcrumbsMenu items={items} handleClick={mockClickHandler} />)
    const item1 = getByText('nocodeapp1')
    expect(mockClickHandler.mock.calls.length).toBe(0)
    fireEvent.click(item1)
    expect(mockClickHandler.mock.calls.length).toBe(1)
    // the first argument of the first call
    expect(mockClickHandler.mock.calls[0][0]).toEqual('/workspaces/wsid1')

    const item2 = getByText('nocodeapp2')
    fireEvent.click(item2)
    expect(mockClickHandler.mock.calls.length).toBe(2)
    // the first argument of the second call
    expect(mockClickHandler.mock.calls[1][0]).toEqual('/workspaces/wsid2')

    mockClickHandler.mockClear()
  })

  it('displays the correct totalItems, itemsType and environment in the footer message', () => {
    const totalItems = 10
    const itemsType = 'apps'
    const environment = 'myWorkspace'
    const { getByText } = render(<BreadcrumbsMenu {...{ items, totalItems, itemsType, environment }} />)
    const showingMessage = getByText(`Showing ${items.length} of ${totalItems} ${itemsType}`)
    expect(showingMessage).toBeInTheDocument()
    const environmentLink = getByText(`View full list in ${environment}`)
    expect(environmentLink).toBeInTheDocument()
  })

  it('calls the handleClick callback function with the correct environmentHref when clicking on the footer item link', () => {
    const environment = 'myEnvironment'
    const environmentHref = 'some/fancy/path'
    const { getByText } = render(
      <BreadcrumbsMenu {...{ items, environment, environmentHref, handleClick: mockClickHandler }} />
    )
    const link = getByText(`View full list in ${environment}`)
    expect(mockClickHandler.mock.calls.length).toBe(0)
    fireEvent.click(link)
    expect(mockClickHandler.mock.calls.length).toBe(1)
    expect(mockClickHandler.mock.calls[0][0]).toEqual(environmentHref)
  })
})
