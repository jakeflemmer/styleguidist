/** @format */

import React from 'react'
import PropTypes from 'prop-types'
import { Menu } from '@blueprintjs/core'
import pluralize from 'pluralize'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import './styles.scss'

/** @type { import('./types').BreadcrumbMenu } */
export const BreadcrumbsMenu = ({
  items,
  itemsType,
  environment,
  environmentHref,
  maxItems,
  totalItems = 0,
  handleClick = () => {}
}) => {
  const itemsLength = items.length > maxItems ? maxItems : items.length
  return (
    <Menu className="breadcrumbs-menu">
      {items.slice(0, maxItems).map(({ icon, href, ...itemProps }, key) => (
        <Menu.Item
          icon={<FontAwesomeIcon icon={icon} fixedWidth />}
          {...{ key, ...itemProps }}
          onClick={() => handleClick(href)}
        />
      ))}

      <Menu.Item
        tagName="footer"
        onClick={() => handleClick(environmentHref)}
        text={
          <div className="full-list">
            <p>
              Showing {itemsLength} of {totalItems} {pluralize(itemsType, totalItems)}
            </p>
            <a>View full list in {environment}</a>
          </div>
        }
      />
    </Menu>
  )
}

BreadcrumbsMenu.defaultProps = {
  items: [],
  itemsType: 'workspace',
  maxItems: 10,
  environment: 'default workspace',
  environmentHref: '/',
  totalItems: 0
}

BreadcrumbsMenu.propTypes = {
  items: PropTypes.array,
  itemsType: PropTypes.string,
  totalItems: PropTypes.number,
  environment: PropTypes.string,
  environmentHref: PropTypes.string,
  handleClick: PropTypes.func
}
