/** @format */

import React from 'react'
import { shallow } from 'enzyme'

import Breadcrumb from '../breadcrumbs'

describe('Breadcrumb', () => {
  // links
  it('renders link to environment when no workspace id or application id passed in', () => {
    const wrapper = shallow(<Breadcrumb params={{}} />)
    const breadcrumbsNavList = wrapper.find('ul.breadcrumbs-nav')
    expect(breadcrumbsNavList).toHaveLength(1)

    const listNavItems = breadcrumbsNavList.at(0).find('li')
    expect(listNavItems).toHaveLength(1)
    const linksInFirstItem = listNavItems.at(0).find('a')
    expect(linksInFirstItem).toHaveLength(1)
    expect(linksInFirstItem.at(0).props()['href']).toBe('/#')
  })

  it('renders links to environment and workspace, when workspace id passed in', () => {
    const wrapper = shallow(<Breadcrumb params={{ workspaceId: '1' }} />)
    const breadcrumbsNavList = wrapper.find('ul.breadcrumbs-nav')
    expect(breadcrumbsNavList).toHaveLength(1)

    const listNavItems = breadcrumbsNavList.at(0).find('li')
    expect(listNavItems).toHaveLength(2)

    const linksInFirstItem = listNavItems.at(0).find('a')
    expect(linksInFirstItem).toHaveLength(1)
    expect(linksInFirstItem.at(0).props()['href']).toBe('/#')

    const linksInSecondItem = listNavItems.at(1).find('a')
    expect(linksInSecondItem).toHaveLength(1)
    expect(linksInSecondItem.at(0).props()['href']).toBe('/#/workspaces/1')
  })

  // TODO: once breadcrumbs component is hooked up to the API, verify that the environment, workspace,
  // and application names are displayed correctly
  it('renders links to environment, workspace, and application when workspace id and application id passed in', () => {
    const wrapper = shallow(<Breadcrumb params={{ workspaceId: '1', applicationId: '1' }} />)
    const breadcrumbsNavList = wrapper.find('ul.breadcrumbs-nav')
    expect(breadcrumbsNavList).toHaveLength(1)

    const listNavItems = breadcrumbsNavList.at(0).find('li')
    expect(listNavItems).toHaveLength(3)

    const linksInFirstItem = listNavItems.at(0).find('a')
    expect(linksInFirstItem).toHaveLength(1)
    expect(linksInFirstItem.at(0).props()['href']).toBe('/#')

    const linksInSecondItem = listNavItems.at(1).find('a')
    expect(linksInSecondItem).toHaveLength(1)
    expect(linksInSecondItem.at(0).props()['href']).toBe('/#/workspaces/1')

    const linksInThirdItem = listNavItems.at(2).find('a')
    expect(linksInThirdItem).toHaveLength(1)
    expect(linksInThirdItem.at(0).props()['href']).toBe('/#/workspaces/1/applications/1')
  })

  // dropdown
  it('renders dropdown caret when user on workspace level', () => {
    const wrapper = shallow(<Breadcrumb params={{ workspaceId: '1' }} />)
    // check last li in breadcrumbs nav list
    const lastNavListItem = wrapper
      .find('ul.breadcrumbs-nav')
      .find('li')
      .at(1)
    expect(lastNavListItem.find('.dropdown-caret')).toHaveLength(1)
  })

  it('renders dropdown caret when user on application level', () => {
    const wrapper = shallow(<Breadcrumb params={{ workspaceId: '1', applicationId: '1' }} />)
    // check last li in breadcrumbs nav list
    const lastNavListItem = wrapper
      .find('ul.breadcrumbs-nav')
      .find('li')
      .at(2)
    expect(lastNavListItem.find('.dropdown-caret')).toHaveLength(1)
  })

  //   // TODO
  //   it('doesnt render dropdown caret when application id passed in, and there is only 1 application', () => {})
  //   it('doesnt render dropdown caret when workspace id passed in, and there is only 1 workspace', () => {})
  // it('renders settings button when user is on workspace level', () => {})
  // it('renders settings/data model/promotion buttons when user is on application level', () => {})
  // TODO: might want to test and make sure that it marks the correct active link
})
