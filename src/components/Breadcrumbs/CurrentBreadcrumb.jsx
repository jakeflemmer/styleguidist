/** @format */

import React from 'react'
import { BreadcrumbsMenu } from './Menu/BreadcrumbsMenu'
import PropTypes from 'prop-types'
import { Breadcrumb, Popover, Position } from '@blueprintjs/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import classnames from 'classnames'

import './styles.scss'

// customize rendering of last breadcrumb
/** @type {import('./types').RenderCurrentBreadcrumb } */
export const CurrentBreadcrumb = ({ text, icon, href, children, handleClick }) => {
  // TODO why do we need this `single` class name???? const classes = classnames('current-item', { single: (items.length === 1) })
  return (
    <span className="breadcrumb-parent">
      <Popover minimal position={Position.BOTTOM} usePortal={false}>
        <Breadcrumb
          icon={icon && <FontAwesomeIcon icon={icon} fixedWidth className="current-icon" />}
          onClick={() => handleClick(href)}
        >
          {/* <span className={classes}>{text}</span> */}
          <span className="current-item"> {text}</span>
          {children && <FontAwesomeIcon icon="caret-down" />}
        </Breadcrumb>
        {children}
      </Popover>
    </span>
  )
}

CurrentBreadcrumb.defaultProps = {
  handleClick: () => {},
  children: null
}

CurrentBreadcrumb.propTypes = {
  text: PropTypes.string,
  icon: PropTypes.string,
  href: PropTypes.string,
  handleClick: PropTypes.func,
  // we expect a BreadcrumbMenu to be passed as a child
  children: PropTypes.node
}
